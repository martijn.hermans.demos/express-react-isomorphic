import Express from "express";
import Http from "http";

import router from "./router";

const app = Express();
const server = Http.createServer(app);
app.use(Express.static("./dist"));

server.listen(process.env.PORT || 3000);

server.on("error", console.error);
server.on("listening", () => {
  const addr = server.address();
  const bind = typeof addr === "string" ? `pipe ${addr}` : `port ${addr.port}`;
  console.log(`Listening on ${bind}`);
});

app.use(router);
