import { Router } from "express";
import React from "react";
import ReactDOM from "react-dom/server";
import createDocument from "./create-document";

import Routes from "@shared/config/routes";
import { getRoute } from "@shared/utils/get-route";

const router = Router();
export default router;

router.get("*", async (req, res) => {
  const { matches, Component, getInitialProps } =
    getRoute(req.originalUrl, Routes) || {};

  let initialProps = {};

  if (getInitialProps) {
    initialProps = await getInitialProps({
      req: { params: matches }
    });
  }

  const html = ReactDOM.renderToString(<Component {...initialProps} />);

  res.send(
    createDocument({
      html,
      jsSrc: "/assets/js/main.js",
      initialProps
    })
  );
});
