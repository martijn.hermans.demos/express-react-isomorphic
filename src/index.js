import React from "react";
import ReactDOM from "react-dom";
import Router from "./router";
import { createBrowserHistory } from "history";

const initialProps = window.__INITIAL_PROPS__;
const history = createBrowserHistory();

ReactDOM.hydrate(
  <Router initialProps={initialProps} history={history} />,
  document.querySelector("#react-app")
);
