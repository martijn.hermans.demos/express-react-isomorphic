import React, { useCallback } from "react";
import useHistory from "@shared/hooks/use-history";

export default function Link({ href, children, ...props }) {
  const history = useHistory();
  const onClick = useCallback(
    event => {
      event.preventDefault();
      history.push(href);
    },
    [href, history]
  );
  return (
    <a href={href} onClick={onClick} {...props}>
      {children}
    </a>
  );
}
