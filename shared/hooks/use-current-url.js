import { useMemo, useState, useEffect } from "react";

export default function useCurrentUrl(history) {
  let [url, setUrl] = useState(history.location.pathname);
  useEffect(() => {
    return history.listen(location => {
      setUrl(location.pathname);
    });
  }, [history, setUrl]);
  return url;
}
