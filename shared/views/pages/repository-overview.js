import React from "react";
import * as Api from "@shared/utils/api";
import RepositoryList from "@shared/components/repository-list";

export default function RepositoryOverviewPage({ query, repositories }) {
  return (
    <div className="container">
      <h3>{query}</h3>
      <RepositoryList repositories={repositories} />
    </div>
  );
}

RepositoryOverviewPage.getInitialProps = async () => {
  const query = "react";

  const repositories = await Api.fetchRepositories(query);

  return { query, repositories };
};
