import React from "react";
import * as Api from "@shared/utils/api";
import Link from "../../components/link";

export default function RepositoryDetailPage({ repository }) {
  return (
    <div className="container">
      <h3>{repository.name}</h3>
      <p>{repository.description}</p>
      <Link href="/">Back to overview</Link>
    </div>
  );
}

RepositoryDetailPage.getInitialProps = async ({ req }) => {
  const { owner, repo } = req.params;

  const repository = await Api.fetchRepository(owner, repo);

  return { repository };
};
